package fr.formation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.model.User;
import fr.formation.service.UserService;

@RestController
@CrossOrigin
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService service;

	@GetMapping("/{name}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public User getByUsername(@PathVariable String name) {
		return service.getByUsername(name).orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Le nom d'utilisateur n'existe pas"));
	}

	@PostMapping("")
	public User createUser(@RequestBody User u) {
		return service.create(u);
	}
}
