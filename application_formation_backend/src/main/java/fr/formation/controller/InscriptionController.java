package fr.formation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.model.User;
import fr.formation.model.dto.UserDTO;
import fr.formation.service.UserService;

@RestController
@CrossOrigin
@RequestMapping("/inscription")
public class InscriptionController {
	
	@Autowired
	UserService userService;

	@PostMapping("")
	public String inscription(@RequestBody UserDTO dto) {
		
		userService.inscription(new User(dto.getUsername(),dto.getPassword()));
		return "Utilisateur créer";
	}
}
