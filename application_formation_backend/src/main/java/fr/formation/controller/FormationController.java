package fr.formation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.model.Formation;
import fr.formation.service.FormationService;

@RestController
@CrossOrigin
@RequestMapping("/api/formation")
public class FormationController {
	@Autowired
	private FormationService formationService;
	
	@GetMapping("")
	public List<Formation> findAll(){
		return formationService.findAll();	
	}

	@GetMapping("/{id}")
	public Formation getById(@PathVariable String libelle) {
		return formationService.getById(libelle)
								.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La formation n'existe pas"));
	}
	
	@PostMapping("")
	public void create(@RequestBody Formation f) {
		if(formationService.getById(f.getLibelle()).isPresent())
			throw new ResponseStatusException(HttpStatus.CONFLICT, "La formation existe déjà");
		formationService.create(f);
	}
	
	@PutMapping("")
	public void update(@RequestBody Formation f) {
		formationService.update(f);
	}
	
	@DeleteMapping("")
	public void delete(@RequestBody Formation f) {
		formationService.delete(f).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La formation n'existe pas"));
	}
	
	@DeleteMapping("/{libelle}")
	public void delete(@PathVariable String libelle) {
		formationService.delete(libelle).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La formation n'existe pas"));
	}
}
