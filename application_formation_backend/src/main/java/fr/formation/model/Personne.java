package fr.formation.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Personne {

	// Attributes
	@Id
	private String id;

	private String dateInscription;
	
	private String formationSouhaitee;
	
	private String nom;

	private String prenom;
	
	private String password;
	
	private String mail;
	
	private String telephone;
	
	private LocalDate dateNaissance;
	
	private String adresse;

	@Enumerated(EnumType.STRING)
	private TypePersonne type;

	@ManyToOne
	@JoinColumn(name = "filiere_id")
	@JsonIgnoreProperties("stagiaires")
	private Filiere filiere;

	@OneToMany(mappedBy = "formateur")
	@JsonIgnoreProperties("formateur")
	private List<Module> modules;

	
	// Constructors
	public Personne() {

	}

	public Personne(String id, String nom, String prenom, String password, String mail, String telephone,
			LocalDate dateNaissance, String adresse, TypePersonne type, Filiere filiere, List<Module> modules
			, String dateInscription,String formationSouhaitee) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.password = password;
		this.mail = mail;
		this.telephone = telephone;
		this.dateNaissance = dateNaissance;
		this.adresse = adresse;
		this.type = type;
		this.filiere = filiere;
		this.modules = modules;
		this.dateInscription = dateInscription;
		this.formationSouhaitee = formationSouhaitee;
	}

	// Getters
	public String getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getPassword() {
		return password;
	}

	public String getMail() {
		return mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public String getAdresse() {
		return adresse;
	}

	public TypePersonne getType() {
		return type;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public List<Module> getModules() {
		return modules;
	}

	
	public String getDateInscription() {
		return dateInscription;
	}
	

	public String getFormationSouhaitee() {
		return formationSouhaitee;
	}

	// Setters
	public void setId(String id) {
		this.id = id;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public void setType(TypePersonne type) {
		this.type = type;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}
	
	
	public void setDateInscription(String dateInscription) {
		this.dateInscription = dateInscription;
	}
	
	public void setFormationSouhaitee(String formationSouhaitee) {
		this.formationSouhaitee = formationSouhaitee;
	}

	// Methods
	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", telephone="
				+ telephone + ", dateNaissance=" + dateNaissance + ", adresse=" + adresse + ", type=" + type + "]";
	}
	
}
