package fr.formation.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Module {

	@Id
	@GeneratedValue(generator = "module_gen")
	@GenericGenerator(name = "module_gen",
		strategy = "fr.formation.model.generators.ModuleIdGenerator"
			)
	private String id;

	private String libelle;

	private LocalDate dateDebut;

	private LocalDate dateFin;

	@ManyToOne
	@JoinColumn(name = "personne_id")
	@JsonIgnoreProperties("modules")
	private Personne formateur;

	@ManyToOne
	@JoinColumn(name = "filiere_id")
	@JsonIgnoreProperties("modules")
	private Filiere filiere;

	public Module() {

	}

	public Module(String id, String libelle, LocalDate dateDebut, LocalDate dateFin, Personne formateur,
			Filiere filiere) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.formateur = formateur;
		this.filiere = filiere;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public Personne getFormateur() {
		return formateur;
	}

	public void setFormateur(Personne formateur) {
		this.formateur = formateur;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

}
