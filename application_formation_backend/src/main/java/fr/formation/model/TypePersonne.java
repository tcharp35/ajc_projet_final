package fr.formation.model;

public enum TypePersonne {
	STAGIAIRE("stagiaire"), FORMATEUR("formateur"), GESTIONNAIRE("gestionaire"), NOUVEL_INSCRIT("nouvel_inscrit");

	private String libelle;

	TypePersonne(String libelle) {
		this.libelle = libelle;
	}

	public String toString() {
		return this.libelle;
	}

	public static TypePersonne fromString(String libelle) {
		for (TypePersonne tp : values()) {
			if (tp.libelle.equals(libelle))
				return tp;
		}
		return null;
	}

}
