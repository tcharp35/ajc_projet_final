package fr.formation.model.generators;

import java.io.Serializable;
import java.util.stream.Stream;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import fr.formation.model.Filiere;
import fr.formation.model.Formation;

public class FiliereIdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		Filiere filiereToCreate = (Filiere) object;
		Formation formationAssoc = filiereToCreate.getFormation();
		
		String query = "SELECT id FROM Filiere WHERE id LIKE " + "'%" + '_' + formationAssoc.getLibelle() + "'";
		Stream<String> ids = session.createQuery(query).stream();
		int max = ids.map(id -> id.replace("_"+formationAssoc.getLibelle(), ""))
					.mapToInt(Integer::parseInt)
					.max()
					.orElse(0);
		
		String id = String.valueOf(max+1) + "_" + formationAssoc.getLibelle();
		System.out.println("=====================================");
		System.out.println("Key generated :" + id);
		System.out.println("=====================================");
		
		return id;
	}

}
