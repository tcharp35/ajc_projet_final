package fr.formation.model.generators;

import java.io.Serializable;
import java.util.stream.Stream;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import fr.formation.model.Module;

public class ModuleIdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		Module moduleToCreate = (Module) object;
		String id = moduleToCreate.getFiliere().getId() + "_" + moduleToCreate.getLibelle();
		System.out.println("=====================================");
		System.out.println("Key generated :" + id);
		System.out.println("=====================================");
		return id;
	}

}
