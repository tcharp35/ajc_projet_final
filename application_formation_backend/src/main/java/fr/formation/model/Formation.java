package fr.formation.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Formation {

	@Id
	private String libelle;
	
	@OneToMany(mappedBy = "formation")
	@JsonIgnoreProperties("formation")
	private List<Filiere> filieres;

	public Formation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Formation(String libelle) {
		super();
		this.libelle = libelle;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Filiere> getFilieres() {
		return filieres;
	}

	public void setFilieres(List<Filiere> filieres) {
		this.filieres = filieres;
	}
	
	
}
