package fr.formation.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class User {

	@Id
	private String username;

	private String password;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	@JsonIgnoreProperties("user")
	private List<UserRole> roles;

	public User() {
		super();
	}
	
	public User(String username,String password) {
		this.username = username;
		this.password = password;
	}

	public User(String username, String password, List<UserRole> roles) {
		super();
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<UserRole> getRoles() {
		return roles;
	}

	public void setRole(List<UserRole> roles) {
		this.roles = roles;
	}

//	@JsonIgnore
//	public Collection<? extends GrantedAuthority> getRolesAsAuthorities() {
//		return this.roles.stream().map(r -> new SimpleGrantedAuthority(r.getRole().name()))
//				.collect(Collectors.toList());
//	}

	public UserDetails toUserDetails() {
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		for(UserRole role : roles) {
			GrantedAuthority ga = new SimpleGrantedAuthority(role.getRole().name());
			authorities.add(ga);
		}		
		
		return new org.springframework.security.core.userdetails.User(this.username, this.password,authorities);
	}

}
