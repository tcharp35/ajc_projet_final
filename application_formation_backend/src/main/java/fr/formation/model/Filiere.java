package fr.formation.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
//@SequenceGenerator(name = "filiere_gen", sequenceName = "filiere_seq", initialValue = 100, allocationSize = 1)
public class Filiere {

	@Id
	@GeneratedValue(generator = "filiere_gen")
	@GenericGenerator(name = "filiere_gen",
	strategy = "fr.formation.model.generators.FiliereIdGenerator"
		)
	private String id;

	@ManyToOne
	private Formation formation;
	
	@OneToMany(mappedBy = "filiere")
	@JsonIgnoreProperties("filiere")
	private List<Module> modules;

	@OneToMany(mappedBy = "filiere")
	@JsonIgnoreProperties("filiere")
	private List<Personne> stagiaires;

	public Filiere() {

	}

	public Filiere(String id, List<Module> modules, List<Personne> stagiaire) {
		super();
		this.id = id;
		this.modules = modules;
		this.stagiaires = stagiaire;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Personne> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<Personne> stagiaire) {
		this.stagiaires = stagiaire;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

}
