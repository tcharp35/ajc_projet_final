package fr.formation.model.dto;

import java.time.LocalDate;
import java.util.List;

import fr.formation.model.Filiere;
import fr.formation.model.Module;
import fr.formation.model.Personne;

public class FiliereDTO {

	private String id;


	private List<Module> modules;

	private List<Personne> stagiaires;

	private LocalDate dateDebut;
	private LocalDate dateFin;

	public FiliereDTO() {

	}

	public FiliereDTO(String id, List<Module> modules, List<Personne> stagiaires, LocalDate dateDebut,
			LocalDate dateFin) {
		super();
		this.id = id;
		this.modules = modules;
		this.stagiaires = stagiaires;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}

	public FiliereDTO(String id, List<Module> modules, List<Personne> stagiaires) {
		super();
		this.id = id;
		this.modules = modules;
		this.stagiaires = stagiaires;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Personne> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<Personne> stagiaires) {
		this.stagiaires = stagiaires;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	public static FiliereDTO fromFiliere(Filiere f) {
		return new FiliereDTO(f.getId(), f.getModules(), f.getStagiaires());
	}

}
