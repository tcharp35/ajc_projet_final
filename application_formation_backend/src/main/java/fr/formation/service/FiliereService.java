package fr.formation.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.controller.exception.BindStagiaireToFiliereException;
import fr.formation.model.Filiere;
import fr.formation.model.Module;
import fr.formation.model.Personne;
import fr.formation.model.dto.FiliereDTO;
import fr.formation.repository.FiliereRepository;

@Service
public class FiliereService {
	
	@Autowired
	private FiliereRepository fr;
	
	@Autowired
	private PersonneService ps;

	public void create(Filiere f) {
		if(!ps.makeAllStagiaire(f.getStagiaires())) throw new BindStagiaireToFiliereException();
		Filiere persistedFiliere = this.fr.save(f);
		for(Personne p : f.getStagiaires()) {
			ps.bindStagiaireToFiliere(p, persistedFiliere);
		}
	}

	public List<Filiere> findAll() {
		return this.fr.findAll();
	}
	
	public Optional<Filiere> getById(String id) {
		return this.fr.findById(id);
	}
	
	public void update(Filiere f) {
		this.fr.save(f);
	}
	
	public Optional<Boolean> delete(Filiere f) {
		try {
			this.fr.delete(f);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<Boolean> delete(String id) {
		try {
			this.fr.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<FiliereDTO> getByIdWithDetail(String id) {
		Optional<Filiere> optF = fr.findById(id);
		if(optF.isEmpty()) return Optional.empty();
		FiliereDTO dto = FiliereDTO.fromFiliere(optF.get());
		LocalDate minDate = null;
		LocalDate maxDate = null;
		for(Module m : dto.getModules()) {
			if(minDate == null || minDate.isAfter(m.getDateDebut())) minDate = m.getDateDebut();
			if(maxDate == null || maxDate.isBefore(m.getDateFin())) maxDate = m.getDateFin();
		}
		dto.setDateDebut(minDate);
		dto.setDateFin(maxDate);
		
		return Optional.of(dto);
	}
	
	public void setStagiairesOfFiliere(List<Personne> pList,String filiereId) {
		Filiere f = new Filiere();
		f.setId(filiereId);
		if(!ps.makeAllStagiaire(pList)) throw new BindStagiaireToFiliereException();
		ps.bindStagiairesToFiliere(pList, f);
	}}
