package fr.formation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.model.Filiere;
import fr.formation.model.Personne;
import fr.formation.model.TypePersonne;
import fr.formation.repository.PersonneRepository;

@Service
public class PersonneService {

	@Autowired
	private PersonneRepository pr;

	public void create(Personne p) {
		this.pr.save(p);
	}

	public List<Personne> findAll() {
		return this.pr.findAll();
	}

	public Optional<Personne> getById(String id) {
		return pr.findById(id);
	}

	public void update(Personne p) {
		this.pr.save(p);
	}

	public Optional<Boolean> delete(Personne p) {
		try {
			this.pr.delete(p);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public Optional<Boolean> delete(String id) {
		try {
			this.pr.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public List<Personne> getFormateursOfFiliere(Filiere f) {
		return pr.findFormateursOfFiliere(f);
	}

	/*
	 * /!\ Cette méthode ne lancera pas d'exception si la personne n'est pas un
	 * stagiaire Attention à gérer ce comportement avant d'appeler cette méthode
	 */
	public void bindStagiairesToFiliere(List<Personne> pList, Filiere f) {
		for (Personne p : pList) {
			pr.bindPersonneToFiliere(p.getId(), f);
		}
	}

	/*
	 * /!\ Cette méthode ne lancera pas d'exception si la personne n'est pas un
	 * stagiaire Attention à gérer ce comportement avant d'appeler cette méthode
	 */
	public void bindStagiaireToFiliere(Personne p, Filiere f) {
		pr.bindPersonneToFiliere(p.getId(), f);
	}

	public Boolean makeAllStagiaire(List<Personne> pList) {
		for (Personne p : pList) {
			Personne persistentPersonne = pr.getById(p.getId());
			if (!TypePersonne.NOUVEL_INSCRIT.equals(persistentPersonne.getType()))
				return false;
			persistentPersonne.setType(TypePersonne.STAGIAIRE);
			this.update(persistentPersonne);
		}
		return true;
	}
}
