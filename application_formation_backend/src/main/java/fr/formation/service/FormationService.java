package fr.formation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.model.Formation;
import fr.formation.repository.FormationRepository;

@Service
public class FormationService {
	@Autowired
	private FormationRepository formationRepo;
	
	public void create(Formation f) {
		this.formationRepo.save(f);
	}
	
	public List<Formation> findAll(){
		return formationRepo.findAll();
	}

	public Optional<Formation> getById(String libelle){
		return formationRepo.findById(libelle);
	}
	
	public void update(Formation f) {
		this.formationRepo.save(f);
	}
	
	public Optional<Boolean> delete(Formation f) {
		try {
			this.formationRepo.delete(f);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<Boolean> delete(String libelle) {
		try {
			this.formationRepo.deleteById(libelle);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	
}
