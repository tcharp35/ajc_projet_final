package fr.formation.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.formation.model.Role;
import fr.formation.model.User;
import fr.formation.model.UserRole;
import fr.formation.repository.UserRepository;
import fr.formation.repository.UserRoleRepository;

@Service
public class UserService {

	@Autowired
	UserRepository ur;

	@Autowired
	UserRoleRepository urr;

	@Autowired
	PasswordEncoder passwordEncoder;

	public Optional<User> getByUsername(String name) {
		return ur.findByUsername(name);
	}

	// Meme méthode pour update et create pour l'instant
	public User create(User u) {
		u.setPassword(passwordEncoder.encode(u.getPassword()));

		ur.save(u);

		// Roles
		for (UserRole userRole : u.getRoles()) {
			userRole.setUser(u);

			create(userRole);
		}
		return u;
	}

	public UserRole create(UserRole userRole) {
		return urr.save(userRole);
	}
	
	public void inscription(User user) {
		UserRole defaultRole = new UserRole(user,Role.ROLE_USER);
//		
//		List<UserRole> listRole = new ArrayList<>();
//		listRole.add(defaultRole);
//		
//		user.setRole(listRole);
		
		user.setRole(Arrays.asList(defaultRole));
		
		create(user);
	}
}
