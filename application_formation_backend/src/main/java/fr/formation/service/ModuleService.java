package fr.formation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.model.Filiere;
import fr.formation.model.Module;
import fr.formation.repository.ModuleRepository;

@Service
public class ModuleService {
	
	@Autowired
	private ModuleRepository mr;

	public void create(Module m) {
		this.mr.save(m);
	}
	
	public List<Module> findAll() {
		return this.mr.findAll();
	}
	
	public Optional<Module> getById(Integer id) {
		return this.mr.findById(id);
	}
	
	public void update(Module m) {
		this.mr.save(m);
	}
	
	public Optional<Boolean> delete(Module m) {
		try {
			this.mr.delete(m);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<Boolean> delete(Integer id) {
		try {
			this.mr.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public List<Module> findByFiliereId(String id) {
		Filiere f = new Filiere();
		f.setId(id);
		return mr.findByFiliere(f);
	}

	public List<Module> getModuleWithoutFormateur() {
		return mr.findByFormateurIsNull();
	}
}
