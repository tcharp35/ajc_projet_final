package fr.formation.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.model.Formation;

public interface FormationRepository extends JpaRepository<Formation, String> {

}
