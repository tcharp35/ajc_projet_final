package fr.formation.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.formation.model.Filiere;
import fr.formation.model.Personne;

public interface PersonneRepository extends JpaRepository<Personne, String> {
	@Modifying
	@Query("UPDATE Personne p SET p.filiere = :f WHERE p.id=:pId AND p.type='STAGIAIRE'")
	@Transactional
	void bindPersonneToFiliere(@Param("pId") String personneId, @Param("f") Filiere filiere);

	@Query("SELECT p FROM Personne p JOIN p.modules m WHERE p.type = 'FORMATEUR' AND m.filiere = :f")
	List<Personne> findFormateursOfFiliere(@Param("f") Filiere f);
}
