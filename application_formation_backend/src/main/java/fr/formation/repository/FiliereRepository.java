package fr.formation.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.model.Filiere;

public interface FiliereRepository extends JpaRepository<Filiere, String> {

}
