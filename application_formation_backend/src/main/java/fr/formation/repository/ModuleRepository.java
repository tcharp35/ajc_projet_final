package fr.formation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.model.Filiere;
import fr.formation.model.Module;

public interface ModuleRepository extends JpaRepository<Module, Integer> {

	List<Module> findByFiliere(Filiere f);

	List<Module> findByFormateurIsNull();
}
