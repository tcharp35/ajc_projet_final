- N'oubliez pas de modifier le fichier application.properties avec vos identifiants de bdd/ url de bdd
- Remarquez la propriété server.port: le tomcat associé à spring boot lancera l'application sur ce port là.


Documentation de l'api REST d'une application de formation




///////// Personne \\\\\\\\\




GET /api/personne
	Description: Renvoie la listes des personnes présentes en BDD
	Format de retour: 
[ 
	{
	 	"id": 100,
        "nom": "nom1",
        "prenom": "prenom1",
        "type": "STAGIAIRE",
        "filiere": {...},
        "modules": []
     },
     ...
]
        
GET /api/personne/:id
	Description: Renvoie la personne correspondant a l'id en paramètre.
	Retour 404 si l'id ne correspond pas à une personne
	Format de retour: 
{
    "id": 100,
    "nom": "nom1",
    "prenom": "prenom1",
    "type": "STAGIAIRE",
    "filiere": {...},
    "modules": []
}

POST /api/personne
	Description: Ajoute une personne en BDD selon l'objet passé dans le body de la requète
	Format attendu: 
{
	"nom":string,
	"prenom":string,
	"type":string ("STAGIAIRE"/"FORMATEUR"),
	"filiere":{"id":int}
}
	
PUT /api/personne
	Description: Modifie la personne correspondant au body de la requète
	Format attendu:
{
	"id":int,
	"nom":string,
	"prenom":string,
	"type":string ("STAGIAIRE"/"FORMATEUR"),
	"filiere":{"id":int}
}

DEL /api/personne/:id
	Description: Supprime la personne correspondant à l'id passé en paramètre
	Retour 404 si l'id ne correspond pas à une filiere
	
	
GET /api/personne/formateurs?filiere_id=:id
	Description: Retourne la liste des formateurs affecté à une filiere passé en paramètre
	Paramètres: {filiere_id} l'id de la filiere
	Format de retour: 
[ 
	{
        "id": 100,
        "nom": "nom1",
        "prenom": "prenom1",
        "type": "FORMATEUR",
        "filiere": null,
        "modules": [...]
 	},
 	...
]




///////// Filiere \\\\\\\\\




GET /api/filiere
	Description: Renvoie la listes des filiere présentes en BDD
	Format de retour: 
[ 
	{
	    "id": 100,
	    "libelle": "filiere1",
	    "modules": [...],
	    "stagiaires": [...]
	},
	...
]
    
    
GET /api/filiere/:id
	Description: Renvoie la filiere correspondant a l'id en paramètre.
	Retour 404 si l'id ne correspond pas à une filiere
	Format de retour: 
{
    "id": 100,
    "libelle": "filiere1",
    "modules": [...],
    "stagiaires": [...]
}

POST /api/filiere
	Description: Ajoute une filiere en BDD selon l'objet passé dans le body de la requète
	Retour 400 si la liste de stagiaires comprend des type="FORMATEUR"
	Format attendu: 
{
    "libelle": string,
    "stagiaires": [
    	{"id":int},
    	...
    ]
}
	
PUT /api/filiere
	Description: Modifie la filiere correspondant au body de la requète
	Format attendu:
{
    "id": 100,
    "libelle": string
}

DEL /api/filiere/:id
	Description: Supprime la filiere correspondant à l'id passé en paramètre
	Retour 404 si l'id ne correspond pas à une filiere
	
GET api/filiere/detail/:id
	Description: Renvoie la filiere correspondant a l'id en paramètre avec des détails (date de début et fin selon les modules associés).
	Retour 404 si l'id ne correspond pas à une filiere
	Format de retour: 
{
    "id": 100,
    "libelle": "filiere1",
    "modules": [...],
    "stagiaires": [...],
    "dateDebut":"01-01-2022",
    "dateDebut":"02-01-2022"
}

PUT /api/filiere/stagiaires/:id
	Description: Affecte la liste de personnes passé dans le body à la filière passé en paramètre
	Format attendu:
[ 
	{
	 	"id": 100,
        "nom": "nom1",
        "prenom": "prenom1",
        "type": "STAGIAIRE"
     },...
]




///////// Module \\\\\\\\\




GET /api/module
	Description: Renvoie la listes des modules présentes en BDD
	Format de retour: 
[ 
	{
	    "id": 100,
	    "libelle": "module1",
	    "dateDebut": "2022-01-01",
	    "dateFin": "2022-01-02",
	    "formateur": {...},
	    "filiere": {...}
	},
	...
]
    
    
GET /api/module/:id
	Description: Renvoie le module correspondant a l'id en paramètre.
	Retour 404 si l'id ne correspond pas à une filiere
	Format de retour: 
{
    "id": 100,
    "libelle": "module1",
    "dateDebut": "2022-01-01",
    "dateFin": "2022-01-02",
    "formateur": {...},
    "filiere": {...}
}

POST /api/module
	Description: Ajoute un module en BDD selon l'objet passé dans le body de la requète
	Format attendu: 
{
    "libelle": string,
    "dateDebut": date
    "dateFin": date,
    "formateur": {"id":int},
    "filiere": {"id":int}
}
	
PUT /api/module
	Description: Modifie le module correspondant au body de la requète
	Format attendu:
{
    "id": int,
    "libelle": string,
    "dateDebut": date
    "dateFin": date,
    "formateur": {"id":int},
    "filiere": {"id":int}
}

DEL /api/module/:id
	Description: Supprime le module correspondant à l'id passé en paramètre
	Retour 404 si l'id ne correspond pas à un module
	
GET /api/module/filiere/:id
	Descrition: Renvoie les modules d'une filière selon l'id de la filière passé en paramètre
	Format de retour:
[ 
	{
	    "id": 100,
	    "libelle": "module1",
	    "dateDebut": "2022-01-01",
	    "dateFin": "2022-01-02",
	    "formateur": {...},
	    "filiere": {...}
	},
	...
]
  
GET /api/module/formateurMissing
	Description: Renvoie la liste des modules n'ayant de pas de formateur
	Format de retour:
[ 
	{
	    "id": 100,
	    "libelle": "module1",
	    "dateDebut": "2022-01-01",
	    "dateFin": "2022-01-02",
	    "formateur": null,
	    "filiere": {...}
	},
	...
]



