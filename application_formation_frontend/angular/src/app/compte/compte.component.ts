import { Component, OnInit } from '@angular/core';
import { AbstractControl, ValidatorFn, FormControl, FormGroup, Validators } from '@angular/forms';

import { Personne } from '../interfaces/personne';
import { PersonneService } from '../services/personne.service';

function passwordValidator(pwd : string | undefined) : ValidatorFn{
  return (control: AbstractControl) => {
    return control.value === pwd ? null : {bad: control.value}
  }
}

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.css']
})
export class CompteComponent implements OnInit {

  type : string | null = localStorage.getItem("TYPE");
  uId : string | null = localStorage.getItem("id");
  utilisateur : Personne | null = null;

  // Password form
  passwordForm : FormGroup = new FormGroup({
    oldPasswordInput: new FormControl('', [Validators.required, ]),
    newPasswordInput: new FormControl('',Validators.required),
  });

  constructor(
    private ps: PersonneService
  ) { }

  ngOnInit(): void {

    if (this.uId != null){
        this.ps.getById(this.uId).subscribe((utilisateur : Personne) => {
          this.utilisateur = utilisateur;
          this.passwordForm.controls["oldPasswordInput"].setValidators([passwordValidator(utilisateur.password)]);
        });
    }
  }

  submit(){
    console.log('ok');
  }
}
