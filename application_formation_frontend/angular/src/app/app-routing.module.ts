import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompteComponent } from './compte/compte.component';
import { FiliereComponent } from './filiere/filiere.component';
import { FilieresComponent } from './filieres/filieres.component';
import { FiliereGestionComponent } from './gestion/filiere-gestion/filiere-gestion.component';
import { FormationGestionComponent } from './gestion/formation-gestion/formation-gestion.component';
import { ModuleGestionComponent } from './gestion/module-gestion/module-gestion.component';
import { PageComponent } from './gestion/page/page.component';
import { StagiaireGestionComponent } from './gestion/stagiaire-gestion/stagiaire-gestion.component';
import { HomeComponent } from './home/home.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ValidationComponent } from './inscription/validation/validation.component';
import { ModuleComponent } from './module/module.component';
import { ModulesComponent } from './modules/modules.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  
  {path: 'filieres', component: FilieresComponent},
  {path: 'filieres/:id', component: FiliereComponent},

  {path:'inscription', component:InscriptionComponent},
  {path:'validation', component: ValidationComponent},

  {path: 'modules', component: ModulesComponent},
  {path: 'modules/:id', component: ModuleComponent},
  
  {path: 'pageGestion', component: PageComponent,
    children:[
      {path: 'formations', component: FormationGestionComponent, outlet: 'gestion'},
      {path: 'filieres', component: FiliereGestionComponent, outlet: 'gestion'},
      {path: 'modules', component: ModuleGestionComponent, outlet: 'gestion'},
      {path: 'stagiaires', component: StagiaireGestionComponent, outlet: 'gestion'}
    ]},

  {path: 'mon-compte', component: CompteComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
