import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Personne } from 'src/app/interfaces/personne';
import { PersonneService } from 'src/app/services/personne.service';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})
export class ValidationComponent implements OnInit {

  id:any=localStorage.getItem('id');
  nom:string ="";
  prenom:string ="";
  mdp:string|undefined='';
  

  constructor(private ps :PersonneService) {
    
    this.ps.getById(this.id).subscribe(res=> {
      console.log(res);
      this.nom= res.nom;
      this.prenom = res.prenom;
      this.mdp = res.password;
    });
    
    
   }

  ngOnInit(): void {
  }


}
