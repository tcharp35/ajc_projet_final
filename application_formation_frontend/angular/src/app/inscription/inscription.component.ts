import { Component, OnInit, Type } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { asapScheduler, switchMap } from 'rxjs';
import { Formation } from '../interfaces/formation';
import { Personne } from '../interfaces/personne';
import { FormationService } from '../services/formation.service';
import { PersonneService } from '../services/personne.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  formations:Formation[]=[];
  userForm! : FormGroup;

  constructor(private personneService: PersonneService, private router : Router, private formBuilder: FormBuilder, private fs : FormationService) {
    this.fs.getAll().pipe(switchMap(formations => formations)).subscribe(res => this.formations.push(res));
   }

  

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.userForm = this.formBuilder.group({
    nom: ['', Validators.required],
     prenom: ['', Validators.required],
     mail : ['', [Validators.required, Validators.email]],
     telephone: ['', Validators.pattern(/\d{10}/g)],
     adresse: ['', Validators.required],
     password : new FormControl('',Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/)),
     formation: ['', Validators.required],
     type: ['STAGIAIRE'],
     ddn:[]
     
    })
  }
 
  onSubmitForm() {
    const formValue = this.userForm.value;
    const Nom :string = formValue['nom'];
    const Prenom :string = formValue['prenom'];
    const Mail = formValue['mail'];
    const Telephone = formValue['telephone'];
    const Adresse = formValue['adresse'];
    //const Password = formValue['password'];
    const Formation = formValue['formation'];
    const Type = formValue['type'];
    const ddn = formValue['ddn']; //YYYY-MM-JJ de type string
    
    var dateInscription = new Date().toLocaleDateString("fr");
    var pass = this.generer_password();
    
    var identifiant = Prenom.charAt(0).toLowerCase() +Nom.toLowerCase();
    //console.log(identifiant)
    

    var personne={   
    id: identifiant, 
    nom:Nom,
    prenom: Prenom,
    password: pass,
    mail: Mail,
    telephone: Telephone,
    adresse: Adresse,
    type:'NOUVEL_INSCRIT',
    dateNaissance:ddn,
    dateInscription:dateInscription,
    formationSouhaitee:this.selectedFormation
   
  };

   // this.personneService.addPersonne(this.personne);
    console.log(personne)
    this.personneService.post(personne).subscribe(res => 
    {console.log("Utilisateur ajouté");
    localStorage.setItem('id',personne.id);
    this.router.navigate(['/validation']);
    }) 
  }

  generer_password() {
    var ok = 'azertyupqsdfghjkmwxcvbn23456789AZERTYUPQSDFGHJKMWXCVBN';
    var pass = '';
    var longueur = 5;
    for(let i=0;i<longueur;i++){
        var wpos = Math.round(Math.random()*ok.length);
        pass+=ok.substring(wpos,wpos+1);
    }
    return pass;
  }

  handleChange(event:any){
    this.selectedFormation = event.target.value;
    console.log(this.selectedFormation);
    
    
  }
  selectedFormation='';
}
