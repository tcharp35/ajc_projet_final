import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { GestionModule } from './gestion/gestion.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { FilieresComponent } from './filieres/filieres.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FiliereComponent } from './filiere/filiere.component';
import { HomeComponent } from './home/home.component';
import { ModulesComponent } from './modules/modules.component';
import { ModuleComponent } from './module/module.component';
import { FooterComponent } from './footer/footer.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ValidationComponent } from './inscription/validation/validation.component';
import { CompteComponent } from './compte/compte.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    FilieresComponent,
    FiliereComponent,
    HomeComponent,
    ModulesComponent,
    ModuleComponent,
    FooterComponent,
    InscriptionComponent,
    FooterComponent,
    ValidationComponent,
    CompteComponent
 
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    GestionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
