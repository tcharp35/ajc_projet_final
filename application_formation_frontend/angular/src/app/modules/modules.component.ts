import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Filiere } from '../interfaces/filiere';
import { FiliereService } from '../services/filiere.service';
import { Module } from '../interfaces/module';
import { ModuleService } from '../services/module.service';
import { Personne } from '../interfaces/personne';
import { PersonneService } from '../services/personne.service';
import { switchMap, filter } from 'rxjs/operators';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {
  modules: Module[] = [];
  selectedModules : Module[] = [];
  filieres: Filiere[] = [];

  // Filtrage
  filtreLibelle : string = "";
  filtreFiliere : string = "";

  // Type de l'utilisateur connecte
  type : string | null = localStorage.getItem("TYPE");

  // Liste stagiaires 
  stagiaires : Personne[] = [];

  constructor(
    private ms: ModuleService,
    private fs: FiliereService,
    private ps: PersonneService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.ms.getAll()
      .subscribe((modules: Module[]) => {
        this.modules = modules;
        this.selectedModules = modules;
      });
    this.fs.getAll()
      .subscribe((filieres: Filiere[]) => {
        this.filieres = filieres
      });
      
    this.ps.getAll()
    .pipe(
      switchMap((personnes) => personnes),
      filter(personne => personne.type === "STAGIAIRE")
    )
    .subscribe(value =>{
      this.stagiaires.push(value)
    });
  }

  // onView(id?: string) {
  //   this.router.navigate(['/modules', id])
  // }

  onEdit(id?: string) {
    this.router.navigate(['/modules'])
  }

  onDelete(id?: string) {
    if (id) {
      this.ms.delete(id)
      .subscribe( () => {
        this.selectedModules = this.modules.filter(module => module.id !== id);
        this.modules = this.modules.filter(module => module.id !== id);
      })
    }

  }

  onSelect(){
    this.selectModules();
  }

  onWriting(){
    this.selectModules();
  }

  selectModules(){
    var res = this.modules;

    // Verifie le filtre libelle
    this.filtreLibelle !== "" ?
      res = res.filter(element => element.libelle.toLowerCase().includes(this.filtreLibelle.toLowerCase()))
      :
      res = res
    ;

    // Verifie le filtre filiere selectionnee
    this.filtreFiliere !== "" ?
      res = res.filter(element => element.filiere && element.filiere.id === this.filtreFiliere)
      :
      res = res
    ;

    this.selectedModules = res;
  }
}
