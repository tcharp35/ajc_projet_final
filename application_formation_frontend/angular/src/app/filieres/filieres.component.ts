import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Filiere } from '../interfaces/filiere';
import { FiliereService } from '../services/filiere.service';
import { Personne } from '../interfaces/personne';
import { PersonneService } from '../services/personne.service';
import { switchMap, filter } from 'rxjs/operators';
import { catchError } from 'rxjs';


@Component({
  selector: 'app-filieres',
  templateUrl: './filieres.component.html',
  styleUrls: ['./filieres.component.css']
})
export class FilieresComponent implements OnInit {

  // Attributs
  filieres: Filiere[] = [];
  datedebut:string="";
  datefin:string="";

 

  // Filtrage
  selectedFilieres: Filiere[] = [];

  filtreFilieresActives : boolean = false;
  filtreLibelle : string = "";
  filtreFormateur : string = "";

  formateurs : Personne[] = [];
  type : string | null = localStorage.getItem("TYPE");

  // Constructeur & Initialisation
  constructor(
    private fs: FiliereService,
    private ps: PersonneService,
    private router: Router
    ) {  }

  message: string = "";
  nbPersonne:number = 0;

  private clearMessage() {
    setTimeout(() => this.message = "", 5000);
  }

  ngOnInit(): void {
    this.fs.getAll().subscribe((filieres: Filiere[]) => {
        this.filieres = filieres;
        this.selectedFilieres = filieres;
        console.log(filieres);
        
      });

    this.ps.getAll()
      .pipe(
        switchMap((personnes) => personnes),
        filter(personne => personne.type === "FORMATEUR")
      )
      .subscribe(value =>{
        this.formateurs.push(value)
      });
  }

  // Code de Christophe
  onView(id?: string) {
    this.router.navigate(['/filieres', id])
  }

  onEdit(id?: string) {
    this.router.navigate(['/filiere-form', id])
  }
  
  onDelete(id?: string) {
    if (id) {
      this.fs.delete(id)
        .pipe(
          catchError((err: HttpErrorResponse) => {
            console.log(err.message);
            this.message = `Suppression impossible 
            (une filière contenant de modules ne peut être supprimée)`;
            this.clearMessage();
            return [];
          })
        )
        .subscribe(() => {
          this.selectedFilieres = this.filieres.filter(filiere => filiere.id !== id);
          this.filieres = this.filieres.filter(filiere => filiere.id !== id);
        })
    }
  }

  // Edit libelle input
  onWriting(){
    this.selectFilieres();
  }

  // Edit checkbox
  onCheck(){
    this.filtreFilieresActives = !this.filtreFilieresActives;
    this.selectFilieres();
  }

  // Select formateur
  onSelect(){
    this.selectFilieres();
  }

  // Travail de filtrage
  selectFilieres(){
    var res = this.filieres;

    // Definit la date
    var date : Date = new Date();
    var month : number | string = date.getMonth()+1;
    month < 10 ? month = '0'+month : month;
    var today : string = date.getFullYear()+"-"+month+"-"+date.getDate();

  // Verifie le filtre libelle
    this.filtreLibelle !== "" ?
      res = res.filter(element => element.id !== undefined && element.id.includes(this.filtreLibelle.toLowerCase()))
      :
      res = res
    ;

    /*// Verifie le filtre filieres actives
    this.filtreFilieresActives ? 
      res = res.filter(element =>
        element.modules?.find(module => module.dateFin > today && module.dateDebut < today))
      :
      res = res
    ;*/

    // Verifie le filtre formateur
    // this.filtreFormateur !== "" ?
    //   res = res.filter(element => element.formateur === this.filtreFormateur);
    //   :
    //   res = res
    // ;

    this.selectedFilieres = res;
  }
}
