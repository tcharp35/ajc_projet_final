import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  type :any= localStorage.getItem('TYPE');
  constructor(private router:Router) { }

  ngOnInit(): void {
  }

    logout(){
      localStorage.clear();
      this.router.navigate(['/']).then(() => {
        window.location.reload();
      });
    }
}
