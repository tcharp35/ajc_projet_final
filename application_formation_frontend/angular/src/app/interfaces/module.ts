import { Filiere } from "./filiere";
import { Personne } from "./personne";

type EntityId = {
  id: string;
}

export interface ModulePost {
  id?: string;
  libelle: string;
  dateDebut: string; //"2022-01-01",
  dateFin: string;
  formateur: EntityId;
  filiere: EntityId;
}

export interface Module {
  id?: string;
  libelle: string;
  dateDebut: string;
  dateFin: string;
  formateur?: Personne;
  filiere?: Filiere;
}
