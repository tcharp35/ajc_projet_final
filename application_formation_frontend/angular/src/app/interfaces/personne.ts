import { Filiere } from "./filiere";

export interface Personne{
    id?:string;
//des points d'interrogations partout pour réaliser un test
    nom:string;
    prenom:string;
    password?:string;
    mail?:string;
    telephone?:string;
    adresse?:string;
    dateNaissance?:string;
    type?:any; //ToDo Sami --modifier le any
    filiere?:Filiere;
    modules?:[];
    
    formationSouhaitee?:string;
    dateInscription?:string;

   
}
