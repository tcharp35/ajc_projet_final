import { Module } from "./module";
import { Personne } from "./personne";


export interface Filiere {
  id?: string;
 // libelle: string;
  modules?: Module[];
  stagiaires?:Personne[];
  formation?:string;
}
