import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/config/settings';
import { Formation } from '../interfaces/formation';

const API = API_URL + '/formation';
@Injectable({
  providedIn: 'root'
})
export class FormationService {

  constructor(private http: HttpClient ) { }

  getAll(): Observable<Formation[]> {
    return this.http.get<Formation[]>(API);
  }

  post(formation : Formation) {
    return this.http.post(API, formation);
  }

  update(formation:Formation) {
    return this.http.put(API,formation);
  }

  delete(id: string) {
    return this.http.delete(API + '/' + id);
  }
  
}
