import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { catchError } from 'rxjs';
import { Formation } from 'src/app/interfaces/formation';
import { FormationService } from 'src/app/services/formation.service';

@Component({
  selector: 'formation-form',
  templateUrl: './formation-form.component.html',
  styleUrls: ['./formation-form.component.css']
})
export class FormationFormComponent implements OnInit {

  formation: Formation = {
    libelle:''
  }

  @Output() postEvent: EventEmitter<string> = new EventEmitter();

  constructor(
    private formationService: FormationService
    ) { }

  message: string = "";

  private clearMessage() {
    setTimeout(() => this.message = "", 5000);
  }

  ngOnInit(): void {
  }

  handleSubmit(){
    console.log('Adding formation...');
      this.formationService.post(this.formation)
        .pipe(
          catchError((err: HttpErrorResponse) => {
            console.log(err.error.error);
            if(err.error.error == 'Conflict'){
              this.message = 'Une formation existe déjà avec ce libelle';
              this.clearMessage();
            }
            return [];
          })
        )
        .subscribe(() => {
          console.log('added '+this.formation.libelle);
          this.postEvent.emit("Création d'une formation : " + this.formation.libelle);
          this.formation.libelle = '';
        })
  }

}
