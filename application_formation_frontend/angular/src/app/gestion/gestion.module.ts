import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ModuleGestionComponent } from './module-gestion/module-gestion.component';
import { FormationGestionComponent } from './formation-gestion/formation-gestion.component';
import { FiliereGestionComponent } from './filiere-gestion/filiere-gestion.component';
import { StagiaireGestionComponent } from './stagiaire-gestion/stagiaire-gestion.component';
import { RouterModule, Routes } from '@angular/router';
import { PageComponent } from './page/page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormationFormComponent } from './formation-form/formation-form.component';


const routes:Routes = [
  // {path: 'stagiaires', component: StagiaireGestionComponent, outlet: 'gestion' },
  // {path: 'modules', component: ModuleGestionComponent, outlet: 'gestion' },
  // {path: 'filieres', component: FiliereGestionComponent, outlet: 'gestion'},
  // {path: 'formations', component: FormationGestionComponent, outlet: 'gestion'}

];

@NgModule({
  declarations: [
    ModuleGestionComponent,
    FormationGestionComponent,
    FiliereGestionComponent,
    StagiaireGestionComponent,
    PageComponent,
    FormationFormComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(routes),
    
    CommonModule
  ],
  exports : [RouterModule]
})
export class GestionModule { }
