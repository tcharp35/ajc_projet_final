import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { filter, switchMap } from 'rxjs';
import { Filiere } from 'src/app/interfaces/filiere';
import { Personne } from 'src/app/interfaces/personne';
import { FiliereService } from 'src/app/services/filiere.service';
import { FormationService } from 'src/app/services/formation.service';
import { PersonneService } from 'src/app/services/personne.service';

@Component({
  selector: 'stagiaire-gestion',
  templateUrl: './stagiaire-gestion.component.html',
  styleUrls: ['./stagiaire-gestion.component.css']
})
export class StagiaireGestionComponent implements OnInit {

   test="";
  personne:Personne={
    nom:'',
    prenom:'',
    

  }
  filiere:Filiere = {
    stagiaires:[],
    id:""
  }
  filieres:Filiere[]=[];
  personnes:Personne[]=[];
  constructor(private ps :PersonneService, private fs:FiliereService, private route : Router) {

    this.ps.getAll().pipe(
      switchMap(personnes => personnes),
      filter(personne => personne.type == "NOUVEL_INSCRIT")
    ).subscribe(res => this.personnes.push(res));

    this.fs.getAll()
    .pipe(
      switchMap(filieres => filieres)
      )
    .subscribe(res => {this.filieres.push(res);}
    );

   }

   

  ngOnInit(): void {
  }
  
  handleChange(event:any){
    this.filiere.id = event.target.value;
    console.log(this.filiere.id);

  }

  handleClick(personne:Personne){
    personne.type = "STAGIAIRE";
    personne.filiere = {
      id:this.filiere.id
    }
    console.log(personne)
    this.ps.update(personne).subscribe(() => {
      console.log('added');
      location.reload();
      this.route.navigate(["pageGestion", { outlets : {gestion:['stagiaires']}}]);
    });
  }

}
