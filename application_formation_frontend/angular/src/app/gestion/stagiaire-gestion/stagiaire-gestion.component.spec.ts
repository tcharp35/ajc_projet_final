import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StagiaireGestionComponent } from './stagiaire-gestion.component';

describe('StagiaireGestionComponent', () => {
  let component: StagiaireGestionComponent;
  let fixture: ComponentFixture<StagiaireGestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StagiaireGestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StagiaireGestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
