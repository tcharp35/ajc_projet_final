import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(["pageGestion", { outlets : {gestion:['formations']}}]);
  }

  navigate(event:any){
    this.router.navigate(["pageGestion", { outlets : {gestion:[event.target.name]}}]);
  }

}
