import { Component, Input, OnInit } from '@angular/core';
import { Formation } from 'src/app/interfaces/formation';
import { FormationService } from 'src/app/services/formation.service';

@Component({
  selector: 'formation-gestion',
  templateUrl: './formation-gestion.component.html',
  styleUrls: ['./formation-gestion.component.css']
})
export class FormationGestionComponent implements OnInit {


  formations: Formation[] = [];

  constructor(
    private formationService: FormationService
    ) { }

  ngOnInit(): void {
    this.updateList()
  }

  updateList(){
    this.formationService.getAll().subscribe(
      (formations:Formation[]) => {
        this.formations = formations;
      }
    )
  }

  
  onDelete(libelle?: string) {
    console.log('deleting formation with libelle : ' + libelle);
    if (libelle) {
      this.formationService.delete(libelle)
        .subscribe(() => {
          this.formations = this.formations.filter(formation => formation.libelle !== libelle);
        })
    }
  }
}
