import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormationGestionComponent } from './formation-gestion.component';

describe('FormationGestionComponent', () => {
  let component: FormationGestionComponent;
  let fixture: ComponentFixture<FormationGestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormationGestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormationGestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
