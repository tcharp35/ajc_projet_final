import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, switchMap } from 'rxjs';
import { Filiere } from 'src/app/interfaces/filiere';
import { Formation } from 'src/app/interfaces/formation';
import { Module, ModulePost } from 'src/app/interfaces/module';
import { Personne } from 'src/app/interfaces/personne';
import { FiliereService } from 'src/app/services/filiere.service';
import { FormationService } from 'src/app/services/formation.service';
import { ModuleService } from 'src/app/services/module.service';
import { PersonneService } from 'src/app/services/personne.service';


@Component({
  selector: 'module-gestion',
  templateUrl: './module-gestion.component.html',
  styleUrls: ['./module-gestion.component.css']
})
export class ModuleGestionComponent implements OnInit {

  filieres:Filiere[]=[];
  module: ModulePost = {
    libelle: '',
    dateDebut: '',
    dateFin: '',
    formateur: {
      id:''
    },
    filiere: {
      id: ''
    }
  };
  formateurs : Personne[] = [];

  
  constructor(
    private ms: ModuleService,
    private fs: FiliereService,
    private ps: PersonneService,
    private formationService : FormationService,
    private router: Router,
    private route : ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.ps.getAll()
    .pipe(
      switchMap(res => res), filter((personne) => personne.type === 'FORMATEUR')
      )
    .subscribe(res => {this.formateurs.push(res);}
    );

    this.fs.getAll()
    .subscribe(res => this.filieres = res)
  }

  handleSubmit() {

    console.log('Add modules...');
    console.log(this.module);
    this.ms.post(this.module)
      .subscribe(() => {
        console.log('add');
        location.reload();
         this.router.navigate(["pageGestion", { outlets : {gestion:['modules']}}])
         ;
             
      })
     
  }

  handleChangeFiliere(event:any){
    
  
         this.module.filiere.id=event.target.value;
       }


  
  handleChangeFormateur(event:any){
    
    
        this.module.formateur.id=event.target.value;
    
 }
}
