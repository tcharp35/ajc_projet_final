import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereGestionComponent } from './filiere-gestion.component';

describe('FiliereGestionComponent', () => {
  let component: FiliereGestionComponent;
  let fixture: ComponentFixture<FiliereGestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiliereGestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereGestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
