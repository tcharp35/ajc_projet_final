import { Component, OnInit } from '@angular/core';
import { filter, switchMap } from 'rxjs';
import { Filiere } from 'src/app/interfaces/filiere';
import { Formation } from 'src/app/interfaces/formation';
import { Personne } from 'src/app/interfaces/personne';
import { FiliereService } from 'src/app/services/filiere.service';
import { FormationService } from 'src/app/services/formation.service';
import { PersonneService } from 'src/app/services/personne.service';


@Component({
  selector: 'filiere-gestion',
  templateUrl: './filiere-gestion.component.html',
  styleUrls: ['./filiere-gestion.component.css']
})
export class FiliereGestionComponent implements OnInit {
  formations:Formation[]=[]; 
  personnes:Personne[]=[];
  filiere: Filiere = {
    stagiaires: [],
    formation:""
  }

   constructor(
    private fs: FiliereService,
    private formationService : FormationService,
    private ps : PersonneService
  ) { }

  ngOnInit(): void {
      
    this.formationService.getAll()
    .subscribe(res => this.formations = res
    );
   }
    

  handleChange(event:any){
    this.filiere.formation = event.target.value;
    console.log(this.filiere.formation);
    
    this.updatePersonnes()

  }

  updatePersonnes(){
    this.personnes=[];
    this.ps.getAll().pipe(
      switchMap(personnes => personnes),
      filter(personne => personne.type == 'NOUVEL_INSCRIT' && personne.formationSouhaitee == this.filiere.formation )
     ).subscribe(res => this.personnes.push(res));
  }

  handleChecked(id:string | undefined, event:any){
    console.log(event.target.checked)
    if (id && event.target.checked){
      const stagiaireToAdd = this.personnes.find(p => p.id == id)
      if(stagiaireToAdd)
        this.filiere.stagiaires?.push(stagiaireToAdd)
    }
    if (id && !event.target.checked){
      const stagiaireToRemove = this.personnes.find(p => p.id == id)
      if(stagiaireToRemove){
        const newList = this.filiere.stagiaires?.filter(p => p.id != id)
        this.filiere.stagiaires = newList
      }
    }
    console.log('personnes checked : ')
    console.log(this.filiere.stagiaires)

  }

  handleSubmit() {
    console.log('Adding filière...');

    // if(this.filiere.stagiaires){
    //   for(let stagiaire of this.filiere.stagiaires){
    //     console.log('changement de type pour ')
    //     console.log(stagiaire)
    //     stagiaire.type = 'STAGIAIRE';
    //     this.ps.update(stagiaire).subscribe(s => {})
    //   }
    // }
    console.log(this.filiere)

    this.fs.post(this.filiere)
      .subscribe(() => {
        console.log('added')
        this.updatePersonnes()
      })
  }
}
