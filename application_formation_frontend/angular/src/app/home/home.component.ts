import { Component, OnInit } from '@angular/core';
import { Personne } from '../interfaces/personne';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonneService } from '../services/personne.service';
import { catchError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  identifiant:any=""; //todo changer en string si ça a été fait dans le back
  password:string="";
  constructor(private ps : PersonneService, private router:Router) { }

  message :string="";
  private clearMessage() {
    setTimeout(()=>this.message ="",3500);
  }
  ngOnInit(): void {
  }
  login(){
    
    /**personneRes = personne résultante de l'appel à l'API**/
    
    this.ps.getById(this.identifiant).pipe(
      catchError((err: HttpErrorResponse) => {
        console.log(err.error.error);
        if(err.error.error == 'Not Found'){
          this.message = 'Une erreur est survenue ! Identifiant introuvable';
          this.clearMessage();
        }
        return [];
      })
    )
    .subscribe(
      personneRes => {
        if(personneRes.password != this.password){
          this.message ='Une erreur est survenue ! Mot de passe incorrect !';
          this.clearMessage();
        }
        if(personneRes.password == this.password){
          console.log(personneRes.prenom + " Bien authentifier");
          console.log(personneRes) 
          if (typeof(personneRes.id) === 'string') localStorage.setItem('id', personneRes.id);
          localStorage.setItem('TYPE', personneRes.type);
          // test console.log(localStorage.getItem(personneRes.nom))
         
          switch (personneRes.type) {
            case 'NOUVEL_INSCRIT' :
            case 'FORMATEUR' :
            case 'GESTIONNAIRE' :
            case 'STAGIAIRE':
             
             // this.router.navigate(['/modules']); //URL à modifier si nécessaire
             this.router.navigate(['/mon-compte'])
             .then(() => {
               window.location.reload();
             });
              break;
            case 'FORMATEUR' :
              this.router.navigate(['/modules'])
             .then(() => {
               window.location.reload();
             });
             break;
            case 'GESTIONNAIRE' :
              this.router.navigate(['/modules'])
              .then(() => {
                window.location.reload();
              });
              break;
          
            default:
              break;
          }   
        }
      }
    );

    //todo faire un routage vers la page suivante
    
    
  }
}
