const API_HOST = 'localhost';
const API_PORT = 8081;
export const API_URL = `http://${API_HOST}:${API_PORT}/api`;